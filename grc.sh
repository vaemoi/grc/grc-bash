#!/bin/bash
# Creating remote repo's from your terminal!
# Arguments -h host -n name -u username -t token --g github --b bitbucket --p private

HOST=bitbucket
NAME=`basename $(pwd)`
USERNAME=`git config --get user.username`
GITHUBTOKEN=
BITBUCKETTOKEN=
PRIVATE=false

function get_host() {
  echo -n "host ($HOST): "
  read TEMP_HOST
  if [[ -n $TEMP_HOST ]]; then
      let HOST=$TEMP_HOST
  fi
}

function get_name() {
  echo -n "repo name ($NAME): "
  read TEMP_NAME
  if [[ -n $TEMP_NAME ]]; then
      let NAME=$TEMP_NAME
  fi
}

function get_username() {
  echo -n "host username ($USERNAME): "
}

function get_token() {
  echo -n "auth token/passwd: "
  read TEMP_TOKEN
  if [[ -n $TEMP_TOKEN ]]; then
    if [[ $HOST == "github" ]]; then
      let GITHUBTOKEN=$TEMP_TOKEN
    fi
    if [[ $HOST == "bitbucket" ]]; then
      let BITBUCKETTOKEN=$TEMP_TOKEN
    fi
  else
    echo "Error token not specified"
    exit 1
  fi
}

function github() {
  if [[ -z TOKEN ]]; then
    get_token
  fi

  if [[ -z NAME ]]; then
    get_name
  fi

  if [[ -z HOST ]]; then
    get_host
  fi

  curl -H "Content-Type: application/json" -X POST \
  -d '{"name":"$NAME", "private":'\"${PRIVATE}\"'}' \
  -u $USERNAME:$GITHUBTOKEN -i https://api.github.com/user/repos
}

function bitbucket() {
  if [[ -z TOKEN ]]; then
    get_token
  fi

  if [[ -z NAME ]]; then
    get_name
  fi

  if [[ -z HOST ]]; then
    get_host
  fi

  curl -H "Content-Type: application/json" -X POST \
  -d '{"scm":"git", "is_private":'\"${PRIVATE}\"'}' \
  -u $USERNAME:$BITBUCKETTOKEN  \
  https://api.bitbucket.org/2.0/repositories/$USERNAME/$NAME
}

function print_options {
  echo "HOST: $HOST"
  echo "NAME: $NAME"
  echo "USER: $USERNAME"
  echo "GITHUBTOKEN: $GITHUBTOKEN"
  echo "BITBUCKETTOKEN: $BITBUCKETTOKEN"
  echo "PRIVATE?: $PRIVATE"
}

function main() {
  print_options

  if [[ $HOST == "github" ]]; then
    github
  elif [[ $HOST == "bitbucket" ]]; then
    bitbucket
  fi
}

while getopts "h:n:u:t:bgp" OPTION; do
  case $OPTION in
    h)
      if [[ $OPTARG == "g" ]]; then
        HOST=github
      elif [[ $OPTARG == "b" ]]; then
        HOST=bitbucket
      else
        echo "ERROR invalid host"
        exit 1
      fi
      ;;
    n)
      NAME=$OPTARG
      ;;
    u)
      USERNAME=$OPTARG
      ;;
    t)
      if [[ $OPTARG == "g" ]]; then
        GITHUBTOKEN=$OPTARG
      else
        BITBUCKETTOKEN=$OPTARG
      fi
      ;;
    b)
      HOST=bitbucket
      ;;
    g)
      HOST=github
      ;;
    p)
      PRIVATE=true
      ;;
  esac
done

main
